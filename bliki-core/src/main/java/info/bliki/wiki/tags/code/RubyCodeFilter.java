package info.bliki.wiki.tags.code;

import java.util.HashMap;

/**
 *
 * @author subwiz
 */
public class RubyCodeFilter extends AbstractCPPBasedCodeFilter implements SourceCodeFormatter {
    
    private static HashMap<String, String> KEYWORD_SET = new HashMap<>();
    private static final String[] KEYWORDS = {
        "BEGIN",
        "END",
        "__ENCODING__",
        "__END__",
        "__FILE__",
        "__LINE__",
        "alias",
        "and",
        "begin",
        "break",
        "case",
        "class",
        "def",
        "defined?",
        "do",
        "else",
        "elsif",
        "end",
        "ensure",
        "false",
        "for",
        "if",
        "in",
        "module",
        "next",
        "nil",
        "not",
        "or",
        "redo",
        "rescue",
        "retry",
        "return",
        "self",
        "super",
        "then",
        "true",
        "undef",
        "unless",
        "until",
        "when",
        "while",
        "yield"
    };
    
    static  {
        for (int i = 0; i < KEYWORDS.length; i++) {
            createHashMap(KEYWORD_SET, KEYWORDS[i]);
        }
    }

    @Override
    public HashMap<String, String> getKeywordSet() {
        return KEYWORD_SET;
    }

    @Override
    public HashMap<String, String> getObjectSet() {
        return null;
    }
    
    public String getName() {
        return "ruby";
    }
    
}
